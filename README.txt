INTRODUCTION
-----------
This module allow you to render Google map images for your Location CCK Fields.

REQUIREMENTS
------------
This module requires the following modules:
 * Location (https://drupal.org/project/location)

INSTALLATION
-----------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Under "Manage Display" tab, set your Location field format to "Gmap Image"

CONFIGURATION
-----------
You can set these options :
 * Dimensions (width & height)
 * Zoom level
 * Map type
 * Show marker or not
 * Retina support or not

MAINTAINERS
-----------
Current maintainers:
 * Maxime Gaul (fra) - https://drupal.org/user/2770045

This project has been sponsored by:
 * Feel & Clic
